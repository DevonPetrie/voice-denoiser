import tensorflow as tf
import tensorflow_io as tfio
import numpy as np

from utils import decode_wav_audio, get_spectrogram_from_waveform

AUTOTUNE = tf.data.AUTOTUNE

class Dataset():
  def __init__(self, clean_filenames, noise_filenames, **config):
    self.clean_filenames = clean_filenames
    self.noise_filenames = noise_filenames

    self.frame_length = config["frame_length"]
    self.frame_step = config["frame_step"]

    self.num_features = config["frame_length"]//2 + 1
    self.num_segments = config["num_segments"]

    self.sample_rate = config["sample_rate"]

    self.input_shape = (self.num_segments, self.num_features, 1)

  def _sample_noise_filename(self):
    return np.random.choice(self.noise_filenames)

  def _add_noise_to_clean_audio(self, clean_audio, noise_signal):
    if len(clean_audio) >= len(noise_signal):
      while len(clean_audio) >= len(noise_signal):
        noise_signal = np.append(noise_signal, noise_signal)

    ## Extract a noise segment from a random location in the noise file
    ind = np.random.randint(0, len(noise_signal) - len(clean_audio))

    noiseSegment = noise_signal[ind: ind + len(clean_audio)]

    speech_power = np.sum(clean_audio ** 2)
    noise_power = np.sum(noiseSegment ** 2)

    noisyAudio = clean_audio + np.sqrt(speech_power / noise_power) * noiseSegment

    return noisyAudio

  # def get_stft_features(self, spectrogram, num_features):
  #   samples = tf.TensorArray(tf.float32, size=len(spectrogram))

  #   zero_vector = tf.zeros((num_features, 1), dtype=tf.float32)

  #   for i in range(len(spectrogram)):
  #     previous_vectors = tf.TensorArray(tf.float32, size=self.num_segments)

  #     for s in range(self.num_segments):
  #       curr_index = i - (self.num_segments+s+1)

  #       if curr_index > 0 and curr_index < len(spectrogram):
  #         previous_vectors = previous_vectors.write(s, spectrogram[curr_index])
  #       else:
  #         previous_vectors = previous_vectors.write(s, zero_vector)

  #     samples = samples.write(i, previous_vectors.stack())

  #   return samples.stack()

  def get_waveform(self, file_path):
    audio_binary = tf.io.read_file(file_path)

    waveform, _ = decode_wav_audio(audio_binary)

    return waveform

  # Length of 1 seconds with sample rate of 44100
  def audio_preprocessing(self, clean_waveform, audio_length=44100):
    noise_waveform = self.get_waveform(self._sample_noise_filename())

    # Trim silence from the start and end of the waveform
    positions = tfio.audio.trim(clean_waveform, axis=0, epsilon=0.1)

    clean_waveform = clean_waveform[positions[0]:positions[1]]

    startPos = 0

    if(len(clean_waveform) > audio_length):
      rangeOfStartPositions = len(clean_waveform) - audio_length

      startPos = tf.experimental.numpy.random.randint(
        0,
        rangeOfStartPositions,
        dtype=tf.dtypes.int32
      )

    clean_waveform = clean_waveform[startPos:startPos+audio_length]

    zero_padding = tf.zeros(
      [audio_length] - tf.shape(clean_waveform)[0],
      dtype=tf.float32
    )

    clean_waveform = tf.cast(clean_waveform, dtype=tf.float32)

    clean_waveform = tf.concat([clean_waveform, zero_padding], 0)

    if len(noise_waveform) < len(clean_waveform):
      noise_zero_padding = tf.zeros(
        [len(clean_waveform)] - tf.shape(noise_waveform),
        dtype=tf.float32
      )

      noise_waveform = tf.concat([noise_waveform, noise_zero_padding], 0)
    else:
      startPos = 0

      if(len(noise_waveform) > audio_length):
        rangeOfStartPositions = len(noise_waveform) - audio_length

        startPos = tf.experimental.numpy.random.randint(
          0,
          rangeOfStartPositions,
          dtype=tf.dtypes.int32
        )

      noise_waveform = noise_waveform[startPos:startPos+len(clean_waveform)]

    noisy_input = clean_waveform + noise_waveform * .75

    return noisy_input, clean_waveform

  def waves_to_spectrogram(self, noisy_waveform, clean_waveform):
    noisy_spec_mag, _ = get_spectrogram_from_waveform(
      waveform=noisy_waveform,
      frame_length=self.frame_length,
      frame_step=self.frame_step,
      input_len=len(noisy_waveform)
    )
    
    clean_spec_mag, _ = get_spectrogram_from_waveform(
      waveform=clean_waveform,
      frame_length=self.frame_length,
      frame_step=self.frame_step,
      input_len=len(clean_waveform)
    )

    return noisy_spec_mag, clean_spec_mag

  # def spectrograms_to_features(self, noisy_spectrogram, clean_spectrogram):
  #   noisy_features = self.get_stft_features(noisy_spectrogram, self.num_features)
  #   clean_features = self.get_stft_features(clean_spectrogram, self.num_features)

  #   return noisy_features, clean_features

  def preprocess_dataset(self):
    files_ds = tf.data.Dataset.from_tensor_slices(self.clean_filenames)

    clean_waveform_ds = files_ds.map(
      map_func=self.get_waveform,
      num_parallel_calls=AUTOTUNE
    )

    waveform_ds = clean_waveform_ds.map(
      map_func=self.audio_preprocessing,
      num_parallel_calls=AUTOTUNE
    )

    spectrogram_ds = waveform_ds.map(
      map_func=self.waves_to_spectrogram,
      num_parallel_calls=AUTOTUNE
    )

    return spectrogram_ds

  # def parallel_audio_processing(self, clean_filename):
  #   noise_filename = self._sample_noise_filename()

  #   clean_audio = self._decode_audio(clean_filename, self.sample_rate)
  #   noise_audio = self._decode_audio(noise_filename, self.sample_rate)

  #   noisy_input = self._add_noise_to_clean_audio(clean_audio, noise_audio)

  #   noisy_input_spectrogram = self._get_spectrogram_from_waveform(waveform=noisy_input, input_len=len(noisy_input))
  #   clean_audio_spectrogram = self._get_spectrogram_from_waveform(waveform=clean_audio, input_len=len(clean_audio))

  #   noisy_input_features = self.get_stft_features(noisy_input_spectrogram, self.num_features)
  #   clean_audio_features = self.get_stft_features(clean_audio_spectrogram, self.num_features)

  #   return noisy_input_features, clean_audio_features

  # def _bytes_feature(self, value):
  #   """Returns a bytes_list from a string / byte."""
  #   if isinstance(value, type(tf.constant(0))):
  #     value = value.numpy()

  #   return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

  # def _get_tf_feature(self, noisy_input_features, clean_audio_features):
  #   noisy_input_features = noisy_input_features.astype(np.float32).tostring()
  #   clean_audio_features = clean_audio_features.astype(np.float32).tostring()

  #   example = tf.train.Example(features=tf.train.Features(feature={
  #       'noise_stft_mag_features': self._bytes_feature(noisy_input_features),
  #       'clean_stft_magnitude': self._bytes_feature(clean_audio_features)}))
  #   return example

  # def create_tf_record(self, *, prefix, subset_size, parallel=True):
  #   counter = 0
  #   pool = multiprocessing.Pool(round(multiprocessing.cpu_count()/2))

  #   for i in range(0, len(self.clean_filenames), subset_size):
  #     tfrecord_filename = "data/records/" + prefix + "/" + str(counter) + ".tfrecords"

  #     # if os.path.isfile(tfrecord_filename):
  #     #   print(f"Skipping {tfrecord_filename}")
  #     #   counter += 1
  #     #   continue

  #     writer = tf.io.TFRecordWriter(tfrecord_filename)
  #     clean_filenames_sublist = self.clean_filenames[i:i+subset_size]

  #     print(f"Processing files from {i} to {i + subset_size}")

  #     if parallel:
  #       out = pool.map(self.parallel_audio_processing, clean_filenames_sublist)
  #     else:
  #       out = [self.parallel_audio_processing(filename) for filename in clean_filenames_sublist]

  #     for o in out:
  #       writer.write(self._get_tf_feature(o[0], o[1]).SerializeToString())

  #     counter += 1
  #     writer.close()