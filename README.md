# Autoencoder voice denoiser
### A voice denoiser making use of a convolutional autoencoder architecture

## Architecture
<img src="./architecture.png" alt="architecture drawing" width="1080">

## Example
<img src="./example-denoise.png" alt="architecture drawing" width="1080">