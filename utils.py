import math
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf

def decode_wav_audio(audio_binary):
  audio, sample_rate = tf.audio.decode_wav(contents=audio_binary)

  audio = tf.reduce_mean(audio, axis=1)

  if len(audio.shape) > 1:
    audio = tf.squeeze(audio, axis=-1)

  return audio, sample_rate

def plot_spectrogram(spectrogram, ax=plt):
  if len(spectrogram.shape) > 2:
    assert len(spectrogram.shape) == 3
    spectrogram = np.squeeze(spectrogram, axis=-1)

  # Convert the frequencies to log scale and transpose, so that the time is
  # represented on the x-axis (columns).
  # Add an epsilon to avoid taking a log of zero.
  log_spec = np.log(spectrogram.T + np.finfo(float).eps)

  height = log_spec.shape[0]
  width = log_spec.shape[1]
  
  X = np.linspace(0, np.size(spectrogram), num=width, dtype=int)
  Y = range(height)

  ax.pcolormesh(X, Y, log_spec)

def spec_to_wav(spec, frame_length, frame_step):
  if len(spec.shape) > 2:
    assert len(spec.shape) == 3
    spec = np.squeeze(spec, axis=-1)

  spec = tf.cast(spec, dtype=tf.complex64)

  return tf.signal.inverse_stft(spec, frame_length=frame_length, frame_step=frame_step)

def wav_to_audio(wav, sample_rate):
  wav_2d = wav[..., tf.newaxis]

  # wav_2d_amped_up = wav_2d * 1000

  return tf.audio.encode_wav(wav_2d, sample_rate)

def get_spectrogram_from_waveform(waveform, frame_length, frame_step, input_len=44100):
  # Zero-padding for an audio waveform with less than input_len samples
  waveform = waveform[:input_len]

  zero_padding = tf.zeros(
    [input_len] - tf.shape(waveform)[0],
    dtype=tf.float32
  )

  # Cast the waveform tensors" dtype to float32.
  waveform = tf.cast(waveform, dtype=tf.float32)

  # Concatenate the waveform with `zero_padding`, which ensures all audio
  # clips are of the same length.
  equal_length = tf.concat([waveform, zero_padding], 0)

  # Convert the waveform to a spectrogram via a STFT.
  spectrogram = tf.signal.stft(equal_length, frame_length=frame_length, frame_step=frame_step)
  
  # Obtain the magnitude of the STFT.
  spectrogram_magnitude = tf.abs(spectrogram)

  # Obtain the phase of the STFT
  spectrogram_phase = tf.math.angle(spectrogram)

  # Pad the spectrogram so it can be reconstructed via the autoencoder
  # spectrogram = tf.pad(spectrogram, ([0, 9], [0, 3]))

  # Add a `channels` dimension, so that the spectrogram can be used
  # as image-like input data with convolution layers (which expect
  # shape (`batch_size`, `height`, `width`, `channels`).
  spectrogram_magnitude = spectrogram_magnitude[..., tf.newaxis]
  spectrogram_phase = spectrogram_phase[..., tf.newaxis]
  
  spectrogram_magnitude = tf.image.resize(spectrogram_magnitude, size=(1024, 128))
  spectrogram_phase = tf.image.resize(spectrogram_phase, size=(1024, 128))

  return spectrogram_magnitude, spectrogram_phase

def get_waveform_from_spec_mag_and_phase(spec_mag, spec_phase, frame_length, frame_step):
  if len(spec_mag.shape) > 2:
    assert len(spec_mag.shape) == 3
    spec_mag = np.squeeze(spec_mag, axis=-1)

  if len(spec_phase.shape) > 2:
    assert len(spec_phase.shape) == 3
    spec_phase = np.squeeze(spec_phase, axis=-1)

  spec_imaginary = spec_mag * np.exp(1j * spec_phase)

  return tf.signal.inverse_stft(spec_imaginary, frame_length=frame_length, frame_step=frame_step)

def batchify_audio(audio, batch_size):
  batches = []
  added_padding = 0

  num_batches = math.ceil(len(audio) / batch_size)

  print("Splitting audio of length (", len(audio), ") into (", num_batches ,") batches")

  for i in range(num_batches):
    if i == num_batches-1:
      batch = audio[i*batch_size:]

      added_padding = [0] * (batch_size-len(batch))

      batches.append(tf.concat([batch, added_padding], 0))
    else:
      batches.append(audio[i*batch_size : (i+1)*batch_size])

  return batches, added_padding