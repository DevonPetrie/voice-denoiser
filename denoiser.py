import tensorflow as tf

import pandas as pd
import numpy as np

from datasets.dataset import Dataset
from utils import get_spectrogram_from_waveform, batchify_audio, get_waveform_from_spec_mag_and_phase, decode_wav_audio, wav_to_audio

class Denoiser(tf.keras.Model):
  def __init__(self):
    super(Denoiser, self).__init__()

    self.DATASET_PATH = "datasets/data/"
    self.COMMON_VOICE_PATH = self.DATASET_PATH + "common-voice/"
    self.URBAN_SOUND_PATH = self.DATASET_PATH + "urban-sound/"

    self.weights_path = "./weights/weights_autoencoder_denoiser"

    self.config = {
      "frame_length": 256,
      "frame_step": round(0.25 * 256),
      "num_segments": 8,
      "sample_rate": 44100
    }

    self.encoder = tf.keras.Sequential([
      tf.keras.layers.Input(shape=(1024, 128, 1)),
      tf.keras.layers.Conv2D(
        filters=32,
        kernel_size=(2, 2),
        strides=(2, 2),
        padding="same",
        activation="relu"
      ),
      tf.keras.layers.Conv2D(
        filters=16,
        kernel_size=(2, 2),
        strides=(2, 1),
        padding="same",
        activation="relu"
      ),
      tf.keras.layers.Conv2D(
        filters=8,
        kernel_size=(2, 2),
        strides=(2, 1),
        padding="same",
        activation="relu"
      ),
    ])

    self.decoder = tf.keras.Sequential([
      tf.keras.layers.Conv2DTranspose(
        filters=8,
        kernel_size=(2, 2),
        strides=(1, 1),
        padding='same',
        activation='relu',
      ),
      tf.keras.layers.Conv2DTranspose(
        filters=16,
        kernel_size=(2, 2),
        strides=(2, 1),
        padding='same',
        activation='relu',
      ),
      tf.keras.layers.Conv2DTranspose(
        filters=32,
        kernel_size=(2, 2),
        strides=(2, 1),
        padding='same',
        activation='relu',
      ),
      tf.keras.layers.Conv2DTranspose(
        filters=1,
        kernel_size=(2, 2),
        strides=(2, 2),
        padding='same',
        activation='relu',
      ),
    ])

    self.build(input_shape=(None, 1024, 128, 1))
    
    self.compile(
      optimizer='adam',
      loss=tf.keras.losses.MeanSquaredError(),
      metrics=[
        tf.keras.metrics.RootMeanSquaredError('rmse')
      ]
    )

  def get_urban_sound_metadata(self, csv_path):
    urbansound_metadata = pd.read_csv(csv_path)

    # shuffle the dataframe
    urbansound_metadata.reindex(np.random.permutation(urbansound_metadata.index))

    return urbansound_metadata

  def get_urban_sound_filenames_by_class_id(self, metadata, audio_path):
    class_ids = np.unique(metadata["classID"].values)
    # print("Number of classes:", class_ids)

    all_files = []
    file_counter = 0

    for c in class_ids:
      per_class_files = metadata[metadata["classID"] == c][["slice_file_name", "fold"]].values

      per_class_files = [audio_path + "/fold" + str(file[1]) + "/" + file[0] for file in per_class_files]

      # print("Class c:", str(c), "has:", len(per_class_files), "files")

      file_counter += len(per_class_files)
      all_files.extend(per_class_files)

    assert len(all_files) == file_counter

    return all_files

  def train(self):
    num_train_samples = 100000
    train_percent = 0.8
    val_percent = 0.1
    test_percent = 0.1

    train_csv = pd.read_csv(self.COMMON_VOICE_PATH+"cv-valid-train.csv")
    val_csv = pd.read_csv(self.COMMON_VOICE_PATH+"cv-valid-dev.csv")
    test_csv = pd.read_csv(self.COMMON_VOICE_PATH+"cv-valid-test.csv")

    clean_train_filenames = [self.COMMON_VOICE_PATH+filename.replace("mp3", "wav") for filename in train_csv["filename"][:num_train_samples]]
    clean_val_filenames = [self.COMMON_VOICE_PATH+filename.replace("mp3", "wav") for filename in val_csv["filename"]]
    clean_test_filenames = [self.COMMON_VOICE_PATH+filename.replace("mp3", "wav") for filename in test_csv["filename"]]

    urban_sound_metadata = self.get_urban_sound_metadata(self.URBAN_SOUND_PATH+"metadata/UrbanSound8K.csv")

    # folds from 0 to 9 are used for training
    urbansound_training_folds = urban_sound_metadata[urban_sound_metadata["fold"] != 10]

    urbansound_filenames = self.get_urban_sound_filenames_by_class_id(urbansound_training_folds, audio_path=self.URBAN_SOUND_PATH+"audio/pcm_16")
    np.random.shuffle(urbansound_filenames)

    # separate noise files for train/validation
    num_train = round(len(urbansound_filenames)*train_percent)
    num_val = round(len(urbansound_filenames)*val_percent)

    urbansound_train_filenames = urbansound_filenames[:num_train]
    urbansound_val_filenames = urbansound_filenames[num_train:num_train+num_val]
    urbansound_test_filenames = urbansound_filenames[num_train+num_val:]

    # print(clean_val_filenames[0])
    # print(urbansound_test_filenames[0])

    # 
    # Create the datasets
    #
    train_dataset = Dataset(clean_train_filenames, urbansound_train_filenames, **self.config)
    val_dataset = Dataset(clean_val_filenames, urbansound_val_filenames, **self.config)
    test_dataset = Dataset(clean_test_filenames, urbansound_test_filenames, **self.config)

    train_dataset = train_dataset.preprocess_dataset()
    val_dataset = val_dataset.preprocess_dataset()
    test_dataset = test_dataset.preprocess_dataset()

    batch_size = 100

    train_dataset = train_dataset.batch(batch_size)
    train_dataset = train_dataset.prefetch(buffer_size=tf.data.AUTOTUNE)

    val_dataset = val_dataset.batch(batch_size)
    val_dataset = val_dataset.prefetch(buffer_size=tf.data.AUTOTUNE)

    test_dataset = test_dataset.batch(batch_size)
    test_dataset = test_dataset.prefetch(buffer_size=tf.data.AUTOTUNE)

    early_stopping_callback = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=50, restore_best_weights=True, baseline=None)

    history = self.fit(
      train_dataset,
      validation_data=val_dataset,
      epochs=5,
      callbacks=[early_stopping_callback]
    )

    metrics = history.history

    self.save_weights(self.weights_path)

    return metrics
  
  def load(self):
    self.load_weights(self.weights_path)

  def process_test_waveform(self, waveform, frame_length, frame_step, batch_size):
    waveform_batches, added_padding = batchify_audio(waveform, batch_size)

    processed = []

    for wav in waveform_batches:
      processed.append(get_spectrogram_from_waveform(
        wav,
        frame_length,
        frame_step
      ))

    return processed, added_padding

  def clean_audio(self, audio_path, output_path):
    test_binary = tf.io.read_file(audio_path)

    noisy_waveform, noisy_waveform_sample_rate = decode_wav_audio(test_binary)

    cleaned_waveform = []

    inputs, added_padding = self.process_test_waveform(noisy_waveform, self.config["frame_length"], self.config["frame_step"], self.config["sample_rate"])

    for input, phase in inputs:
      cleaned_input = self(input[tf.newaxis, ...])

      cleaned_input = tf.image.resize(cleaned_input[0], size=(686, 129))
      phase = tf.image.resize(phase, size=(686, 129))

      cleaned_input_wav = get_waveform_from_spec_mag_and_phase(cleaned_input, phase, self.config["frame_length"], self.config["frame_step"])

      cleaned_waveform = tf.concat([cleaned_waveform, cleaned_input_wav], 0)

    cleaned_waveform = cleaned_waveform[:len(cleaned_waveform)-len(added_padding)]

    noisy_spec, _ = get_spectrogram_from_waveform(
      noisy_waveform,
      self.config["frame_length"],
      self.config["frame_step"]
    )

    cleaned_spec, _ = get_spectrogram_from_waveform(
      cleaned_waveform,
      self.config["frame_length"],
      self.config["frame_step"]
    )

    tf.io.write_file(output_path, wav_to_audio(cleaned_waveform, noisy_waveform_sample_rate))

    return noisy_waveform, cleaned_waveform, noisy_spec, cleaned_spec
  
  def call(self, x):
    encoded = self.encoder(x)
    decoded = self.decoder(encoded)

    return decoded