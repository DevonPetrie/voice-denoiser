import matplotlib.pyplot as plt

from denoiser import Denoiser
from utils import plot_spectrogram 

denoiser = Denoiser()

# Comment out if training
# denoiser.load()

denoiser.encoder.summary()
denoiser.decoder.summary()

# Train
metrics = denoiser.train()

plt.plot(metrics["loss"], label="Train")
plt.plot(metrics["val_loss"], label="Validation")

plt.legend()
plt.ylabel("Loss (MSE)")
plt.xlabel("Epoch")

plt.savefig("./images/train-graph.png")

plt.show()
plt.close()

# Test with the quick brown fox recording
noisy_waveform, cleaned_waveform, noisy_spec, clean_spec = denoiser.clean_audio("./audio/quick-brown-fox.wav", output_path="./audio/quick-brown-fox-cleaned.wav")

rows = 2
cols = 2
n = rows * cols
fig, axes = plt.subplots(rows, cols, figsize=(14, 7))

axes[0, 0].set_title("Input")
axes[0, 1].set_title("Output")

axes[0, 0].plot(noisy_waveform)
axes[0, 1].plot(cleaned_waveform)

plot_spectrogram(noisy_spec, axes[1, 0])
plot_spectrogram(clean_spec, axes[1, 1])

plt.savefig("./images/quick-brown-fox-denoise.png")

plt.show()
plt.close()